		<footer id="footer" role="contentinfo" class="group">
			<div class="wrap">
				<div class="footer-info">
					<img id="robin-porter" src="<?php echo get_template_directory_uri()?>/images/footer-robin-porter.png" alt="Robin Porter"/>
					<a href="https://ca.linkedin.com/in/robin-porter-3288a83a" target="_blank">
						<svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
							 x="0px" y="0px" width="57.6px" height="57.6px" viewBox="0 0 57.6 57.6" xml:space="preserve">
						<g>
							<path fill="#00FFCE" d="M30.5,26.7L30.5,26.7C30.5,26.6,30.5,26.7,30.5,26.7L30.5,26.7z"/>
							<path fill="#00FFCE" d="M0,0v57.6h57.6V0H0z M20.9,42.6h-6.2V24h6.2V42.6z M17.8,21.5L17.8,21.5c-2.1,0-3.4-1.4-3.4-3.2
								c0-1.8,1.4-3.2,3.5-3.2c2.1,0,3.4,1.4,3.5,3.2C21.3,20,20,21.5,17.8,21.5z M43.2,42.6H37v-9.9c0-2.5-0.9-4.2-3.1-4.2
								c-1.7,0-2.7,1.2-3.2,2.3c-0.2,0.4-0.2,0.9-0.2,1.5v10.4h-6.2c0,0,0.1-16.8,0-18.6h6.2v2.6c0.8-1.3,2.3-3.1,5.6-3.1
								c4.1,0,7.1,2.7,7.1,8.4V42.6z"/>
						</g>
						</svg>
					</a>
					<img id="copywriter" src="<?php echo get_template_directory_uri()?>/images/footer-copywriter.png" alt="Copywriter"/>
					<img id="toronto" src="<?php echo get_template_directory_uri()?>/images/footer-toronto.png" alt="Toronto, Ontario"/>
				</div>

				<div class="contact-form">
					<h3>Email Robin</h3>
					<form
						name="contactForm"
						ng-submit="submitContactForm(contactForm.$valid)"
						novalidate
						ng-class="showErrors ? 'show-errors' : '' ">
						<label>
							<span>Your name:</span>
							<input type='text' name="name" ng-model="response.name" required/>
						</label>
						<label>
							<span>Email address:</span>
							<input type='email' name="email" ng-model="response.email" required/>
						</label>
						<label>
							<span>Message:</span>
							<textarea required name="message" ng-model="response.message"></textarea>
						</label>
						<input type='submit' value="Submit"/>
					</form>
				</div>
			</div>
		</footer>
	</div> <!-- end wrapper -->

	<div 
			id="lightbox" 
			ng-click="dismissModal()" 
			ng-class="{'hide':'hide', 'loading':'show', 'show':'show','invisible':'invisible'}[modalStatus]" 
			ng-cloak></div>
	<div
			compile="modalContents"
			ng-cloak></div>

	<?php wp_footer(); ?>
</body>
</html>