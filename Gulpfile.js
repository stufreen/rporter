
/**
 * Gulpscript
 *
 * Used to manage and improve the development process. This
 * script does not need to be run on every deployment of
 * the app.
 */

var gulp            = require('gulp');
var gutil           = require('gulp-util');
var sass            = require('gulp-sass');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('gulp-autoprefixer');
var concat          = require('gulp-concat');
var rename          = require('gulp-rename');
var minifycss       = require('gulp-minify-css');
var uglify          = require('gulp-uglify');
var bourbon         = require('node-bourbon');

// Functions

function handleError(error){
  console.log(error.toString());
  this.emit('end');
  gutil.beep();
}

// Style generation tasks

gulp.task('styles', function() {
    return gulp.src('sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            // sourceComments: true,
            includePaths: ['styles'].concat(
                bourbon.includePaths
            )
        }))
        .on('error', handleError)
        .pipe(autoprefixer("last 3 versions", "> 1%", "ie 8"))
        .pipe(rename(function(path) {
            path.dirname = '';
            path.extname = '.css';
        }))
        .pipe(minifycss({
            keepSpecialComments: '1'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
        // .pipe(browserSync.stream({match: '**/*.css'}));
        // .pipe(livereload());
});

// Scripts generation tasks
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .on('error', handleError)
        // .pipe(gulp.dest('.'));
        // .pipe(browserSync.stream({match: '**/*.css'}));
});

// Setup watcher task

gulp.task('watch', function() {

    gulp.watch('sass/*.scss', ['styles']);
    gulp.watch('js/*.js', ['scripts']);

});

gulp.task('default', ['watch']);