var app = angular.module('rpApp', []);

app.directive( 'fadeOutWithScroll', [ '$window', function(  $window ) {
    return {
        link: function( scope, elem, attrs ){

            elem.css('position', 'fixed');
            var win = angular.element($window);

            win.bind('scroll', function() {

                var doc = document.documentElement;
                var windowScroll = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
                var header = elem.parent();

                if (windowScroll > header[0].offsetHeight / 2){
            		elem.css('opacity', '0');
                }
                else{
                    var scrollPercent = windowScroll / header[0].offsetHeight * 2;
                    elem.css('opacity', 1 - scrollPercent);
                    elem.css('display', 'inherit');
                }
                
            });

            win.bind('resize', function() {
                elementOffset = elem.prop('offsetTop');
                elementHeight = elem.prop('offsetHeight');
            });
        }
    }
} ] );

app.controller("ModalController", ['$scope', '$http', '$timeout', '$sce', function($scope, $http, $timeout, $sce){

    $scope.modalContents = "";

    $scope.modalStatus = 'hide';

    $scope.loadAndShowModal = function(postID){
        $scope.modalStatus = 'loading';
        var ajaxUrl = ajax_params.ajax_url;
        $http({
            method: 'POST',
            url: ajaxUrl,
            params : {
                action: 'route_ajax_request',
                route_to: 'modal.php',
            },
            data : {
                post_id: postID
            }
        }).then(function successCallback(response) {
            $scope.modalContents = response.data;
            $scope.modalStatus = 'invisible';
            $timeout(function(){
                $scope.modalStatus = 'show';
            }, 1);
        }, function errorCallback(response) {
            $scope.modalStatus = 'hide';
        });

    }

    $scope.formResponse = {};

    $scope.showErrors = false;

    $scope.submitContactForm = function(valid){
        if(valid){
            $scope.showErrors = false;
            $scope.modalStatus = 'loading';
            var ajaxUrl = ajax_params.ajax_url;
            $http({
                method: 'POST',
                url: ajaxUrl,
                params : {
                    action: 'route_ajax_request',
                    route_to: 'process-contact-form.php',
                },
                data : {
                    response: $scope.response
                }
            }).then(function successCallback(response) {
                $scope.modalContents = response.data;
                $scope.modalStatus = 'invisible';
                $timeout(function(){
                    $scope.modalStatus = 'show';
                }, 1);
            }, function errorCallback(response) {
                $scope.modalStatus = 'hide';
            });
        }
        else{
            $scope.showErrors = true;
        }
    }

    $scope.dismissModal = function(){
    	$scope.modalStatus = 'invisible';

    	$timeout(function() {
    			$scope.modalStatus = 'hide';
                $scope.modalContents = '';
    	}, 500);
    }
}]);

app.directive( 'positionModal', [ '$window', function(  $window ) {
    return {
        link: function( scope, elem, attrs ){
	          scope.$watch("modalStatus", function() {
	              var doc = document.documentElement;
                  var windowScroll = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
                  var windowHeight = window.innerHeight;
	              elem.css('top', (windowScroll + windowHeight/2)+'px');
	          });
        }
    }
} ] );

app.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
        scope.$watch(
            function(scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function(value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}]);

app.directive( 'initializeSwiper', ['$window', function( $window ) {
    return {
        link: function( scope, elem, attrs ){

            console.log('initialize swiper');

            var win = angular.element($window);

            var options = {
                pagination: '.swiper-pagination',
                slidesPerView: 3,
                paginationClickable: true,
                spaceBetween: 30,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                breakpoints: {
                    768: {
                        slidesPerView: 1,
                    }
                }
            };

            var swiper = new Swiper(elem, options);

            win.bind('resize', function(){
                swiper.destroy(true, true);
                swiper = new Swiper(elem, options);
            });
        }
    }
}]);

app.directive( 'initializeModalSwiper', function( ) {
    return {
        link: function( scope, elem, attrs ){
            var swiper = new Swiper(elem, {
                pagination: '.swiper-modal-pagination',
                paginationClickable: true,
                //allowSwipeToPrev: false,
                //allowSwipeToNext: false
            });
        }
    }
} );