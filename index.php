<?php get_header(); ?>
<section id="work" role="main">
  <div class="wrap">
    <h2>Work</h2>

    <?php
    // The Query
    $args = array(
      'post_type' => 'post'
    );
    $the_query = new WP_Query( $args );

    // The Loop
    if ( $the_query->have_posts() ) :

      if(count($the_query->posts) > 3): ?>
        <div class="swiper-container" initialize-swiper>
        <div class="swiper-wrapper">
      <?php endif;

      while ( $the_query->have_posts() ) :
        $the_query->the_post(); 
        $featured_image = get_field('featured_image', $post->ID) ?>
        <div class="swiper-slide">
          <div class="work-item">
            <div class="work-wrap" ng-click="loadAndShowModal(<?php the_ID() ?>)">
              <div class="work-pic">
                <img src="<?= $featured_image['sizes']['medium'] ?>">
              </div>
              <h3><?php the_title() ?></h3>
              <?php the_excerpt() ?>
            </div>
          </div>
        </div>
      <?php endwhile;

      if(count($the_query->posts) > 3): ?>
        </div>
        <div class="swiper-pagination"></div>
          <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        </div>
      <?php endif;
    endif;
    /* Restore original Post Data */
    wp_reset_postdata();
    ?>
  </div>
</section>
<?php get_footer(); ?>