<?php
// The Query

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$response = $request->response;

//Create the body of the response email
$email = "<html><body>";
$email .= "<h3>RobinPorter.ca Contact Form Response</h3>";
$email .= "<p>Someone has filled out a form on RobinPorter.ca. Their response is as follows.</p>";
foreach($response as $key => $value){
	$email .= "<strong>" . $key . ": </strong>" . $value;
	$email .= "<br/>";
}
$email .= "<p>This is an automated email.</p>";
$email .= "</body></html>";
//Generate headers for both emails
$headers = "From: noreply@robinporter.ca\r\n";
$headers .= "Reply-To: noreply@robinporter.ca\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

//Send responses to the form administrator at ILAC
wp_mail('porter.robin@gmail.com', 'RobinPorter.ca Contact Form Response',  $email, $headers);
?>

<div id="contact-confirm-modal"
	ng-class="{'hide':'hide', 'loading':'hide', 'show':'show','invisible':'invisible'}[modalStatus]">
	<h3>EMAIL SENT</h3>
	<p>Your email has been sent! Robin will respond to you shortly.</p>
	<h4><a href="" ng-click="dismissModal()">OK</a></h4>
</div>