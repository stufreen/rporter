<?php
// The Query

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$args = array(
					'p' => $request->post_id
				);
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();

		?>
		<div id="modal"
			ng-class="{'hide':'hide', 'loading':'hide', 'show':'show','invisible':'invisible'}[modalStatus]" 
			position-modal>
			<div class="closer" ng-click="dismissModal()">
				<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 width="357px" height="357px" viewBox="0 0 357 357" style="enable-background:new 0 0 357 357;" xml:space="preserve">
					<g>
						<g id="close">
							<polygon points="357,35.7 321.3,0 178.5,142.8 35.7,0 0,35.7 142.8,178.5 0,321.3 35.7,357 178.5,214.2 321.3,357 357,321.3 
								214.2,178.5 		"/>
					</g>
				</svg>
			</div>
		<?php

		// check if the repeater field has rows of data
		$media_length = count(get_field('media'));

		if( have_rows('media') ):
		 	// loop through the rows of data
			if($media_length > 1){
				echo '<div class="swiper-container" initialize-modal-swiper><div class="swiper-wrapper">';
			}
			else{
				echo '<div><div>';
			}

	    while ( have_rows('media') ) : 
	    	the_row();
	    	echo "<div class='swiper-slide'>";
	    	echo "<div class='media-container'>";
        $type_of_media = get_sub_field('type_of_media');
        if($type_of_media == 'image'){
        	$image = get_sub_field('image_picker');
        	echo "<div class='media-img-container' style='background-image: url(\"" . $image['sizes']['large'] . "\")'></div>";
        }
        elseif($type_of_media == 'video_url'){
        	the_sub_field('video_url');
        }
        echo "</div></div>";
	    endwhile;

	    echo '</div>';
	    if($media_length > 1){
	    	echo '<div class="swiper-modal-pagination"></div>';
	   	}
	    echo '</div>';
		else :
		    // no rows found
		endif;

		$categories = get_the_category();
		?>
		<h2><?php the_title(); ?></h2>
		<h3><?php echo $categories[0]->cat_name; ?></h3>
		<div><?php the_content(); ?></div>
		</div>
		<?php
	}
} else {
	// no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
?>