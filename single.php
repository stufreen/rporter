<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<script>
			var galleryPostId = <?php the_ID(); ?>;
		</script>
	
		<div class="gallery-description">
			<h1><?php the_title(); ?></h1>
			<h5><?php the_date(); ?></h5>
			<?php the_content(); ?>
		</div><!--end gallery-description-->

		<div class="gallery">
			<div class="gallery-focus" 
				hm-tap="next()" 
				ng-style="{'background-image':'url('+focusImage.sizes.galleryfocus+')'}"
				hm-swipeleft="next()"
				hm-swiperight="previous()">
				<div class="gallery-focus-info" ng-hide="focusImage.caption == ''">{{ focusImage.caption }}</div>		
			</div>
			<ul class="thumbnails">
				<li ng-repeat="image in images">
					<img 	ng-src="{{ image.sizes.thumbnail }}" 
								ng-click="goTo(image)"
								ng-class="image == focusImage ? 'selected' : 'not-selected'"/>
				</li>
			</ul>
		</div><!--end gallery-->
		

	</article>
	<div class="group"> </div>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>