<?php
add_image_size( 'galleryfocus', 1200, 1200 );
add_filter( 'jpeg_quality', create_function( '', 'return 60;' ) );

function rporter_enqueue_scripts()
{
	wp_enqueue_script(
		'angular',
		get_template_directory_uri() . '/js/angular.min.js',
		array(),
		'1.4.8',
		true
	);
	wp_enqueue_script(
		'swiper',
		get_template_directory_uri() . '/bower_components/Swiper/dist/js/swiper.min.js',
		array(),
		'3.3.0',
		true
	);
	wp_enqueue_script(
		'rporter_app',
		get_template_directory_uri() . '/js/app.js',
		array('angular'),
		'1.0.0',
		true
	);
	wp_localize_script( 
		'rporter_app',
		'ajax_params',
		array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'theme_path' => get_template_directory() ) );
}
add_action( 'wp_enqueue_scripts', 'rporter_enqueue_scripts' );

add_action( 'wp_ajax_route_ajax_request', 'route_ajax_request' );
add_action( 'wp_ajax_nopriv_route_ajax_request', 'route_ajax_request' );
function route_ajax_request() {
	$path = get_template_directory() . '/partial/' . htmlspecialchars($_GET['route_to']);
	include( $path );
	die();
}